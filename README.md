# SymAware 🚗

[![License](https://img.shields.io/badge/License-BSD_3--Clause-orange.svg)](https://opensource.org/licenses/BSD-3-Clause)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://black.readthedocs.io/en/stable/the_black_code_style/current_style.html)
[![Pipeline](https://gitlab.com/testsimaware/testsymaware-sisw/badges/main/pipeline.svg)](https://gitlab.com/testsimaware/testsymaware-sisw/-/pipelines)
[![coverage report](https://gitlab.com/testsimaware/testsymaware-sisw/badges/main/coverage.svg)](https://gitlab.com/testsimaware/testsymaware-sisw/-/commits/main)

<!-- Main package description -->

## Welcome to SymAware

[SymAware](https://www.symaware.eu) addresses the fundamental need for a new conceptual framework for awareness in multi-agent systems (MASs) that is compatible with the internal models and specifications of robotic agents and that enables safe simultaneous operation of collaborating autonomous agents and humans.

The SymAware framework will use compositional logic, symbolic computations, formal reasoning, and uncertainty quantification to characterise and support situational awareness of MAS in its various dimensions, sustaining awareness by learning in social contexts, quantifying risks based on limited knowledge, and formulating risk-aware negotiation of task distributions.

### Project Structure

The SymAware project is divided into **N** packages.
Each is responsible for handling a component of the overall architecture.

- [**MPI**](https://gitlab.com/testsimaware/testsymaware-mpi)
- [**SISW**](https://gitlab.com/testsimaware/testsymaware-sisw)

### Architecture scheme

```mermaid
%%{ init : { "flowchart" : { "curve" : "basis" }}}%%
flowchart LR
    env_input{{External inputs from environment}}
    agent_input{{External inputs from another agent}}

    subgraph agent_j[Agent j]
        direction TB
        transmitted_output([Received Communication])
    end

    subgraph human[Human agent]
        direction LR
        goal([Goal])
        preferences([User preferences])
    end

    subgraph agent_i[Agent i]
        direction LR
        perceptual_information[Perceptual Information]
        knowledge[Knowledge]
        received_communication[Received Communication]
        chosen_action[Chosen Action]
        physical_state[Physical State of the system]
        communication_human[Communication Interface with Human Agent]
        communication_agent[Communication Interface with Agent j]

        subgraph situational_awareness[Situational Awareness]
            direction LR
            state([State])
            intent([Intent])
            uncertainty([Uncertainty])
            risk([Risk])
        end
    end

    env_input --> perceptual_information
    agent_input --> received_communication

    received_communication --> knowledge
    received_communication --> situational_awareness

    perceptual_information --> knowledge
    perceptual_information --> communication_agent
    perceptual_information --> situational_awareness

    knowledge --> situational_awareness
    knowledge --> communication_agent

    situational_awareness --> communication_agent
    situational_awareness --> chosen_action
    situational_awareness --> communication_human

    chosen_action --> communication_agent
    chosen_action --> communication_human
    chosen_action --> physical_state

    physical_state --> perceptual_information

    communication_human --> human
    communication_agent --> agent_j

classDef yellow stroke:yellow,stroke-width:1px
classDef red stroke:red,stroke-width:1px
classDef green stroke:green,stroke-width:1px
classDef blue stroke:blue,stroke-width:1px

class state,intent,uncertainty,risk,situational_awareness,knowledge red;
class human,communication_human,goal,preferences green;
class agent_j,communication_agent,transmitted_output,agent_input,received_communication blue;
class env_input,perceptual_information,physical_state yellow;
style chosen_action stroke:magenta,stroke-width:1px;
```

<!-- Specific package description -->

## SISW Package

This repository contains the SISW package of the SymAware project.

> **TODO: add detailed description**  
> E.g.  
> The SISW package addresses **_..._**.

<!-- General information -->

### Installation

> **TODO: add installation instructions**  
> E.g.  
> The SISW package requires Python version 3.9+ and can be installed by running the following command: `pip3 install symaware-sisw`.

### Usage

> **TODO: add usage instructions**  
> E.g.  
> To use the SISW package, run the following command: `python3 -m symaware.api`.

### Documentation

> TODO: add documentation

### Development

If you want to actively contribute to the development of the package, please follow the instructions below.

#### Prerequisites

All the following software should be already present on the machine:

- [Python 3.9+](https://www.python.org/downloads/)
- [pip](https://pip.pypa.io/en/stable/installing/)
- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [tox](https://tox.wiki)

> [!Note]  
> Tox is an optional tools that makes managing the project way easier, since it takes care of testing, linting, creating the documentation and publishing the package.
> This said, every operation can still be carried out by using the hidden components Tox relies on.
> For most commands, both alternatives will be provided: with Tox and without

#### Installation

- Clone the repository: `git clone git@gitlab.com:testsimaware/testsymaware-sisw.git'
- Navigate to the repository: `cd testsymaware-sisw`
  - _Optional:_ Create a virtual environment: `python3 -m venv .venv`
  - _Optional:_ Activate the virtual environment: `source .venv/bin/activate # linux or mac` or `.\.venv\Scripts\activate # windows`
- Install the package in editable mode: `pip install -e .`

#### Running the package

If the package has been installed in editable mode, it is possible to run it with the following command:

```bash
python3 -m symaware.sisw
```

#### Linting and testing

To ensure an high code quality, numerous linting and testing checks have been added to this repository.
Keep in mind that all the checks must succede for any merge request to be accepted.

With Tox, it will take care of all the setup needed for both testing and linting.

```bash
# Run the linter
tox -e lint
# Run the tests
tox -e test
# Or run all at once on all available Python versions
tox
```

Without Tox, the linting and testing additional dependencies must be installed before running all the scripts manually/

```bash
# Install additional dependencies
pip3 install .[lint,test]
# Lint
black --check src tests
pylint src tests
mypy src tests
isort --check-only --diff src tests
# Test
pytest
```

##### Automatically fixing issues with linting

If you encounter some issues with the formatter (_black_) or the import sorter (_isort_) you can let them automatically modify the source files to fix the problem automatically.

With Tox.

```bash
# Fix lint problems
tox -e fixlint
```

Without Tox.

```bash
# Install additional dependencies
pip3 install .[lint]
# Fix lint problems
black src tests
isort src tests
```

## Documentation

The documentation is automatically generated by Sphinx and can be found [here](https://testsimaware.gitlab.io/testsymaware-mpi/).

## Code of Conduct

Contributions to the project are allowed upon consent from the project managers. In order to make a contribution, we highly recommend following the coding advised proposed from the [Google Python Coding standards](https://google.github.io/styleguide/pyguide.html)

## Contact ✉️

- Gregorio Marchesini (gremar@kth.se)
- Arabinda Ghosh (arabinda@mpi-sws.org)
- Zengjie Zhang (z.zhang3@tue.nl)

## Funding information

This project has received funding from the Horizon Europe- the Framework Programme for Research and Innovation, under that grant agreement 101070802.
