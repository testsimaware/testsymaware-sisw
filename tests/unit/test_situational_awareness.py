# pylint: disable=import-error,unused-import,missing-function-docstring,missing-class-docstring,protected-access,unnecessary-lambda-assignment,unnecessary-lambda,redefined-outer-name
from pytest_mock import MockerFixture, mocker

from testsimaware.sisw import SituationAwareness, SystemStatus


class TestSituationalAwareness:
    def test_constructor(self):
        sa = SituationAwareness()
        assert sa.cpu_threshold == 80
        assert sa.disk_threshold == 80
        assert sa.memory_threshold == 80

    def test_add_callback(self):
        callback = lambda x: print(x)
        sa = SituationAwareness()
        sa.add("low_cpu", callback)
        assert len(sa._callbacks["low_cpu"]) == 1
        assert callback in sa._callbacks.get("low_cpu", set())

    def test_add_callback_no_duplicate(self):
        callback = lambda x: print(x)
        sa = SituationAwareness()
        sa.add("low_cpu", callback)
        sa.add("low_cpu", callback)
        assert len(sa._callbacks.get("low_cpu", set())) == 1

    def test_remove_callback(self):
        callback = lambda x: print(x)
        sa = SituationAwareness()
        sa.add("low_cpu", callback)
        sa.remove("low_cpu", callback)
        assert len(sa._callbacks.get("low_cpu", set())) == 0

    def test_remove_callback_not_present(self):
        callback = lambda x: print(x)
        sa = SituationAwareness()
        sa.remove("low_cpu", callback)
        assert len(sa._callbacks.get("low_cpu", set())) == 0

    def test_update(self, mocker: MockerFixture):
        sa = SituationAwareness()
        mock = mocker.patch.object(sa, "_parse_update")
        status = SystemStatus(cpu_percent=0, disk_usage_percent=0, virtual_memory_percent=0)

        sa.update(status)

        assert sa.system_status == status
        mock.assert_called_once()

    def test_cpu(self, mocker: MockerFixture):
        sa = SituationAwareness()
        callback = mocker.Mock()
        sa.add("low_cpu", callback)
        status = SystemStatus(cpu_percent=sa.cpu_threshold, disk_usage_percent=0, virtual_memory_percent=0)

        sa.update(status)

        callback.assert_called_once()
        callback.assert_called_with(status)

    def test_memory(self, mocker: MockerFixture):
        sa = SituationAwareness()
        callback = mocker.Mock()
        sa.add("low_memory", callback)
        status = SystemStatus(cpu_percent=0, disk_usage_percent=0, virtual_memory_percent=sa.memory_threshold)

        sa.update(status)

        callback.assert_called_once()
        callback.assert_called_with(status)

    def test_disk(self, mocker: MockerFixture):
        sa = SituationAwareness()
        callback = mocker.Mock()
        sa.add("low_disk_space", callback)
        status = SystemStatus(cpu_percent=0, disk_usage_percent=sa.disk_threshold, virtual_memory_percent=0)

        sa.update(status)

        callback.assert_called_once()
        callback.assert_called_with(status)

    def test_events(self, mocker: MockerFixture):
        sa = SituationAwareness()
        callback = mocker.Mock()
        sa.add("low_cpu", callback)
        sa.add("low_memory", callback)
        sa.add("low_disk_space", callback)
        status = SystemStatus(
            cpu_percent=sa.cpu_threshold,
            disk_usage_percent=sa.disk_threshold,
            virtual_memory_percent=sa.memory_threshold,
        )

        sa.update(status)

        assert callback.call_count == 3
        callback.assert_called_with(status)
