from typing import Callable, Literal

from .api import SystemStatus
from .util import Publisher

Events = Literal["low_cpu", "low_memory", "low_disk_space"]


class SituationAwareness(Publisher):
    """
    Action class.
    It is responsible for deciding what to do based on the system status.
    If a certain condition is met, it will trigger an event the agent can subscribe to
    in order to perform an action.


    Attributes
    ----------
    _memory_threshold:
        memory threshold in percentage above which the event ``low_memory`` is triggered
    _disk_threshold:
        disk threshold in percentage above which the event ``low_disk_space`` is triggered
    _cpu_threshold:
        cpu threshold in percentage above which the event ``low_cpu`` is triggered
    _system_status:
        last known system status or None if not available
    """

    def __init__(self, memory_threshold: float = 80, disk_threshold: float = 80, cpu_threshold: float = 80):
        super().__init__()
        self._memory_threshold = memory_threshold
        self._disk_threshold = disk_threshold
        self._cpu_threshold = cpu_threshold
        self._system_status: "SystemStatus | None" = None

    def update(self, system_status: SystemStatus):
        """
        A new system status is available, update the current status and parse it
        using the ``_parse_update`` method, that represents the logic of the agent.

        Parameters
        ----------
        system_status:
            new system status
        """
        self._system_status = system_status
        self._parse_update()

    def _parse_update(self):
        """
        Analyze the current system status and trigger the appropriate events.

        Raises
        ------
        ValueError:
            if the system status is not available
        """
        if self._system_status is None:
            raise ValueError("System status not available")
        if self._system_status.virtual_memory_percent >= self._memory_threshold:
            self.notify("low_memory", self._system_status)
        if self._system_status.disk_usage_percent >= self._disk_threshold:
            self.notify("low_disk_space", self._system_status)
        if self._system_status.cpu_percent >= self._cpu_threshold:
            self.notify("low_cpu", self._system_status)

    def add(self, event: Events, callback: Callable[[SystemStatus], None]):
        """
        Add a callback to the publisher.
        The callback must accept a SystemStatus object as parameter, corresponding to the new system status that
        triggered the event.

        Parameters
        ----------
        event:
            event identifier the callback will be invoked for
            Available events:

            - low_memory
            - low_disk_space
            - low_cpu
        callback:
            the callback to add
        """
        self._add(event, callback)

    def remove(self, event: Events, callback: Callable[[SystemStatus], None]):
        """
        Remove a callback from the publisher.

        Parameters
        ----------
        event:
            event identifier the callback was attached to.
            Available events:

            - low_memory
            - low_disk_space
            - low_cpu
        callback:
            the callback to remove
        """
        self._remove(event, callback)

    def notify(self, event: str, *data: SystemStatus):
        """
        Notify the subscribers that a new system status is available.

        Parameters
        ----------
        event:
            event identifier.
        data:
            new system status
        """
        self._notify(event, *data)

    @property
    def memory_threshold(self) -> float:
        """
        Return the memory threshold in percentage above which the event ``low_memory`` is triggered.

        Returns
        -------
        memory threshold in percentage
        """
        return self._memory_threshold

    @property
    def disk_threshold(self) -> float:
        """
        Return the disk threshold in percentage above which the event ``low_disk_space`` is triggered.

        Returns
        -------
        disk threshold in percentage
        """
        return self._disk_threshold

    @property
    def cpu_threshold(self) -> float:
        """
        Return the cpu threshold in percentage above which the event ``low_cpu`` is triggered.

        Returns
        -------
        cpu threshold in percentage
        """
        return self._cpu_threshold

    @property
    def system_status(self) -> "SystemStatus | None":
        """
        Return the last known system status or None if not available.

        Returns
        -------
        last known system status or None if not available
        """
        return self._system_status
