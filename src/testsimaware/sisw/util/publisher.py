from typing import Any, Callable


class Publisher:
    """
    The Publisher interface declares a set of methods for managing subscribers (observers) and notifying them of
    state changes.
    The publisher will invoke the callback provided by the subscriber when a determinate event occurs.

    Attributes
    ----------
    _callbacks : A dictionary of set of callbacks added to this publisher.
                The key for each set is the identifier of the event the callback is interested in.
                Note that each set does not allow duplicates.
    """

    def __init__(self):
        self._callbacks: dict[str, set[Callable]] = {}

    def _add(self, event: str, callback: Callable):
        """
        Add a callback to the publisher.
        From now on, every time the publisher's state changes, it will invoke the callback.
        Keep in mind that duplicate callbacks are not allowed, so calling this method multiple times with the same
        one will not add it multiple times.
        This said, it is easy to create multiple semantically equals callbacks, for example by using lambda functions.

        Parameters
        ----------
        event       : identifier of the event the callback will be invoked for.
        callback    : The callback to attach. It is a callable object that will be invoked when the publisher's state
                    changes.
        """
        if event not in self._callbacks:
            self._callbacks[event] = set()
        self._callbacks[event].add(callback)

    def _remove(self, event: str, callback: Callable):
        """
        Remove a callback from the publisher.
        It won't be invoked anymore when a notification is sent.

        Parameters
        ----------
        event       : identifier of the event the callback was attached to.
        callback  : The callback to remove.
        """
        if event in self._callbacks and callback in self._callbacks[event]:
            self._callbacks[event].remove(callback)

    def _notify(self, event: str, *data: Any):
        """Notify the subscribers of the event.

        Parameters
        ----------
        event : event identifier
        data  : data to invoke the callbacks with
        """
        if event in self._callbacks:
            for callback in self._callbacks[event]:
                callback(*data)
