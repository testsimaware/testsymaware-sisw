import argparse
from typing import TYPE_CHECKING

from .situational_awareness import SituationAwareness

try:
    from ._version import __version__
except ImportError:
    __version__ = "unknown"

if TYPE_CHECKING:
    from argparse import Namespace

    class CommandLineArgs(Namespace):
        """Command line arguments type hint.

        Attributes:
        ----------
        memory_threshold:
            memory threshold in percentage above which the event ``low_memory`` is triggered
        disk_threshold:
            disk threshold in percentage above which the event ``low_disk_space`` is triggered
        cpu_threshold:
            cpu threshold in percentage above which the event ``low_cpu`` is triggered
        """

        memory_threshold: float
        disk_threshold: float
        cpu_threshold: float


def parse_args() -> "CommandLineArgs":
    """
    Parse command line arguments for the script.

    Returns:
    --------
    args:
        A namespace containing the parsed arguments. Namely:
        - number_of_agents : The number of agents in the simulation.
        - simulation_iterations : The number of iterations to run the simulation for.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", action="version", version=__version__)
    parser.add_argument("--memory-threshold", type=float, help="memory threshold in percentage", default=80)
    parser.add_argument("--disk-threshold", type=float, help="disk threshold in percentage", default=80)
    parser.add_argument("--cpu-threshold", type=float, help="cpu threshold in percentage", default=80)
    return parser.parse_args()  # type: ignore


def main():
    """
    Main entry point for the application.
    Can be executed by the command line by running `python -m testsimaware.sisw`.
    """
    args = parse_args()
    sa = SituationAwareness(
        cpu_threshold=args.cpu_threshold, disk_threshold=args.disk_threshold, memory_threshold=args.memory_threshold
    )
    print(sa)


if __name__ == "__main__":
    main()
