import time
from dataclasses import dataclass, field


@dataclass
class SystemStatus:
    """
    Real time data collected from the system.
    It describes the physical state of the system at a given time.

    Attributes
    ----------
    cpu_percent:
        the percentage of CPU usage
    virtual_memory_percent:
        the percentage of virtual memory usage
    disk_usage_percent:
        the percentage of disk usage
    timestamp:
        the unix timestamp of the moment when the data was collected
    """

    cpu_percent: float
    virtual_memory_percent: float
    disk_usage_percent: float
    timestamp: float = field(default_factory=time.time)
